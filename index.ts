import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineComponent } from './src/timeline/timeline.component';
import { SlimScrollModule } from 'ng2-slimscroll';

export * from './src/timeline/timeline.component';

@NgModule({
  imports: [
    CommonModule,
    SlimScrollModule
  ],
  declarations: [
    TimelineComponent,

  ],
  exports: [
    TimelineComponent,

  ]
})
export class TimelineModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TimelineModule,
      providers: []
    };
  }
}
